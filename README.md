# Invoices List Backend with Node js and Express

This project contains the backend portion of the Invoices List application. The backend portion deals with storing the invoices and any changes made to them by the user. Furthermore, the application handles various requests from the frontend by also dealing with authentication using JWT tokens.

Use git clone https://dannydmrxhu@bitbucket.org/dannydmrxhu/invoices-list-fe.git to obtain the frontend portion of this application.

## Starting the backend server

Use node index.js or nodemon index.js to start the backend server.
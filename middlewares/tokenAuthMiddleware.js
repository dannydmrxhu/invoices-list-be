
// Initial Setup
const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('../config');

// Verify token
function authMiddleware(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (bearerHeader !== undefined) {
        // Split Authorization: Bearer <access_token>
        const bearer = bearerHeader.split(' ');
        
        // Get token from array
        const bearerToken = bearer[1];

        jwt.verify(bearerToken, config.jwtSecret, (err, data) => {
            if(err) {
                const error = new Error("Authentication error")
                error.status = 401
                next(error);
            } else {
                next();
            }
        });
    } else {
        const error = new Error('Not allowed to access');
        error.status = 403;
        next(error);
    }
}

module.exports = authMiddleware;
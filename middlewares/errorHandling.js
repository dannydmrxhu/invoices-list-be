
module.exports = function(err, req, res, next) {
    const status = err.status || 400;
    res.status(status).send({
        status,
        errorMessage: err.message
    });
}
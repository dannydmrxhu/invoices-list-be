// Initial Setup
const express = require('express');
const jwt = require('jsonwebtoken');
const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('./config');

// Creating express app
var app = express();
var invoiceRoutes = require('./routes/invoiceRoutes');
var errorHandler = require('./middlewares/errorHandling');

// Setting up CORS and BodyParser
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Storing invoice id's
app.locals.id = 0;

// Invoices 
app.locals.invoices = [
    { id: app.locals.id++, name: 'Oranges', edit: false },
    { id: app.locals.id++, name: 'Apples', edit: false },
    { id: app.locals.id++, name: 'Music', edit: false },
    { id: app.locals.id++, name: 'Ginger', edit: false },
    { id: app.locals.id++, name: 'Notebooks', edit: false }
];

// Useful functions
var usefulFunctions = require('./usefulFunctions/functions');
var isEmpty = usefulFunctions.isEmpty;

// Hardcoded admin data for testing purposes
app.locals.adminData = {
    'user': 'admin',
    'password': 'admin'
};


// Login Check and Authentication
app.post('/login', (req, res, next) => {
    if(!isEmpty(req.body)) {
        if ( req.body.user === app.locals.adminData.user && req.body.password === app.locals.adminData.password ) {
            const token = jwt.sign({ user: app.locals.adminData.user }, config.jwtSecret);
            setTimeout((function() {
                res.status(200).send({
                    'token': token
                });
            }), 1000);

        } else {
            setTimeout((function() {
                const error = new Error("Invalid username or password");
                error.status = 403;
                next(error);
            }), 1000);
        }
    } else {
        const error = new Error("Something unexpected happened");
        error.status = 400;
        next(error);
    }
});


// Router that deals with invoices
app.use('/invoices', invoiceRoutes(app.locals));

// Page not found
app.use((req, res, next) => {
    const error = new Error("Page not found");
    error.status = 404;
    next(error);
});

// NOTE: Error handler middleware must be registred as the last
// middleware in the list, so all exceptions routed to Express
// will be managed
app.use(errorHandler);

// Listening on port 5000
app.listen(5000, () => console.log('Server started on port 5000'));


// Initial setup
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

// JWT Authentication Middleware
var tokenAuthMiddleware = require('../middlewares/tokenAuthMiddleware');

// Useful functions 
var usefulFunctions = require('../usefulFunctions/functions');
var isEmpty = usefulFunctions.isEmpty;

module.exports = function(locals) {
    // NOTE: Register middleware for invoices routes
    // that need to be protected with token authentication
    router.use(tokenAuthMiddleware);

    // Getting the invoices request
    router.get('/', (req, res) => {
        res.status(200).send(
            JSON.stringify(locals.invoices)
        );
    });

    // Storing a new Invoice inserted by the user
    router.post('/', (req, res, next) => {
        if(!isEmpty(req.body)) {
            var newObject = {
                id: locals.id++, name: req.body.name, edit: false
            }
            locals.invoices.unshift(newObject);
            res.status(200).send(
                JSON.stringify(newObject)
            );
        } else {
            const error = new Error("No new invoice provided");
            error.status = 400
            next(error);
        }
    });

    // Deleting an invoice 
    router.delete('/:id', (req, res, next) => {
        const index = locals.invoices.findIndex(x => x.id == req.params.id);

        if(index !== -1) {
            locals.invoices.splice(index, 1);            
            res.status(200).send(
                JSON.stringify(locals.invoices)
            );
        } else {
            const error = new Error("The invoice was not found");
            error.status = 400;
            next(error);
        }
    });

    // Updating the content of an invoice
    router.put('/', (req, res, next) => {
        if(!isEmpty(req.body)) {
            const index = locals.invoices.findIndex(invoice => invoice.id == req.body.id);
        
            if(index !== -1) {
                locals.invoices[index].name = req.body.name;
                res.status(200).send(
                    JSON.stringify(locals.invoices)
                );
            } else {
                const error = new Error("The specified invoice was not found");
                error.status = 400;
                next(error);
            }
        } else {
            const error = new Error("No invoice provided");
            error.status = 400
            next(error);
        }
    });

   return router;
}
